
describe('Curtir e comentar artigos', () => {

    before(() => {
        cy.login(Cypress.env('user').email, Cypress.env('user').password);
        cy.contains('No articles are here... yet.');
    });

    beforeEach(() => {
        cy.mockArticles();
        
        cy.contains('Global Feed').click();
    });

    it('Curtir artigo, deve ficar com 1 curtida', function() {
        cy.get('.pull-xs-right > .btn').should((listOfLike) => {
            expect(listOfLike.first()).to.contain('0');
        });

        cy.get('.ion-heart').first().click();

        cy.get('.pull-xs-right > .btn').should((listOfLike) => {
            expect(listOfLike.first()).to.contain('1');
        });
    });    

    it('Comentar em um artigo', () => {
        cy.get('.preview-link > span').first().click();
        cy.get('.form-control').click();
        cy.get('.btn').click();

        cy.get('p.card-text').should('have.text', 'teste de comentario');
        cy.get('.ion-trash-a').should('be.visible', true);
    });
});
