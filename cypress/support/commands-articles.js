Cypress.Commands.add('mockArticles', () => {
    cy.server()
    cy.route('GET', '**/articles*', 'fixture:articles.json')
})